package com.classpath.kstreamsprocessor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KstreamsProcessorApplication {

    public static void main(String[] args) {
        SpringApplication.run(KstreamsProcessorApplication.class, args);
    }

}
