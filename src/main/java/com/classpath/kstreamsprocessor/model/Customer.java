package com.classpath.kstreamsprocessor.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public  class Customer {
    private String firstName;
    private String lastName;
    private String customerId;
    private String creditCardNumber;
}
