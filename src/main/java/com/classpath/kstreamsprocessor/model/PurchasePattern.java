package com.classpath.kstreamsprocessor.model;

import lombok.*;

import java.util.Date;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode
public class PurchasePattern {

    private String zipCode;
    private String item;
    private Date date;
    private double amount;

}
