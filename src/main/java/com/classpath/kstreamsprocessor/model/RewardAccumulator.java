package com.classpath.kstreamsprocessor.model;

import lombok.*;

@Setter
@Getter
@ToString
@EqualsAndHashCode
@Builder
public class RewardAccumulator {
    private String customerId;
    private double purchaseTotal;
    private int totalRewardPoints;
    private int currentRewardPoints;
    private int daysFromLastPurchase;
}
