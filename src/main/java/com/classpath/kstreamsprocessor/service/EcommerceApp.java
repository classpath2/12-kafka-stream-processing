package com.classpath.kstreamsprocessor.service;


import com.classpath.kstreamsprocessor.model.Purchase;
import com.classpath.kstreamsprocessor.model.PurchasePattern;
import com.classpath.kstreamsprocessor.model.RewardAccumulator;
import com.classpath.kstreamsprocessor.serdes.StreamsSerdes;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.kstream.*;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import java.util.Properties;

@Component
public class EcommerceApp implements CommandLineRunner {

    private static final Logger LOG = LoggerFactory.getLogger(EcommerceApp.class);

    @Override
    public void run(String... args) throws Exception {
        Serde<Purchase> purchaseSerde = StreamsSerdes.PurchaseSerde();
        Serde<PurchasePattern> purchasePatternSerde = StreamsSerdes.PurchasePatternSerde();
        Serde<RewardAccumulator> rewardAccumulatorSerde = StreamsSerdes.RewardAccumulatorSerde();
        Serde<String> stringSerde = Serdes.String();

        StreamsBuilder streamsBuilder = new StreamsBuilder();

        KStream<String,Purchase> purchaseKStream = streamsBuilder.stream("transactions", Consumed.with(stringSerde, purchaseSerde))
                .mapValues(Purchase::maskCreditCard);

        purchaseKStream.print(Printed.<String, Purchase>toSysOut().withLabel("masked"));

        KStream<String, PurchasePattern> patternKStream = purchaseKStream.mapValues(EcommerceApp::buildPurchasePattern);

        //patternKStream.print(Printed.<String, PurchasePattern>toSysOut().withLabel("patterns"));
        patternKStream.to("patterns", Produced.with(stringSerde,purchasePatternSerde));


/*
        streamsBuilder.stream("transactions", Consumed.with(stringSerde, purchaseSerde))
                .mapValues(Purchase::maskCreditCard)
                .mapValues(EcommerceApp::buildPurchasePattern)
                .to("patterns", Produced.with(stringSerde,purchasePatternSerde));
*/

        KStream<String, RewardAccumulator> rewardsKStream = purchaseKStream.mapValues(EcommerceApp::rewardAccumulatorFromPurchase);

        //rewardsKStream.print(Printed.<String, RewardAccumulator>toSysOut().withLabel("rewards"));
        rewardsKStream.to("rewards", Produced.with(stringSerde,rewardAccumulatorSerde));


        KeyValueMapper<String, Purchase, Long> purchaseDateAsKey = (key, purchase) -> purchase.getPurchaseDate().getTime();

        KStream<Long, Purchase> filteredKStream = purchaseKStream.filter((key, purchase) -> purchase.getPrice() > 5.00).selectKey(purchaseDateAsKey);

        filteredKStream.print(Printed.<Long, Purchase>toSysOut().withLabel("purchases"));
        filteredKStream.to("purchases", Produced.with(Serdes.Long(),purchaseSerde));


        Predicate<String, Purchase> isCoffee =
                (key, purchase) ->
                        purchase.getDepartment().equalsIgnoreCase("coffee");

        Predicate<String, Purchase> isElectronics =
                (key, purchase) ->
                        purchase.getDepartment().equalsIgnoreCase("electronics");

        int coffee = 0;
        int electronics = 1;

        KStream<String, Purchase>[] kstreamByDept =
                purchaseKStream.branch(isCoffee, isElectronics);

        kstreamByDept[coffee].to( "coffee",
                Produced.with(stringSerde, purchaseSerde));
        kstreamByDept[electronics].to("electronics",
                Produced.with(stringSerde, purchaseSerde));



        //purchaseKStream.print(Printed.<String, Purchase>toSysOut().withLabel("purchases"));
        //purchaseKStream.to("purchases", Produced.with(stringSerde,purchaseSerde));

        // used only to produce data for this application, not typical usage
        MockDataProducer.producePurchaseData();

        KafkaStreams kafkaStreams = new KafkaStreams(streamsBuilder.build(),getProperties());
        LOG.info("First Kafka Streams Application Started");
        kafkaStreams.start();
        Thread.sleep(65000);
        LOG.info("Shutting down the Kafka Streams Application now");
        kafkaStreams.close();
        MockDataProducer.shutdown();
    }




    private static Properties getProperties() {
        Properties props = new Properties();
        props.put(StreamsConfig.CLIENT_ID_CONFIG, "ECommerce-Streams-Client");
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "order-purchases");
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "ECommerce-Kafka-Streams-App");
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "15.206.203.167:9092");
        return props;
    }

    private static PurchasePattern buildPurchasePattern(Purchase purchase) {
        return PurchasePattern.builder().zipCode(purchase.getZipCode()).item(purchase.getItemPurchased()).date(purchase.getPurchaseDate()).amount(purchase.getPrice() * purchase.getQuantity()).build();
    }

    private static RewardAccumulator rewardAccumulatorFromPurchase(Purchase purchase) {
        return RewardAccumulator.builder()
                    .customerId(purchase.getLastName() + "," + purchase.getFirstName())
                    .purchaseTotal(purchase.getPrice() * (double) purchase.getQuantity())
                    .currentRewardPoints((int) (purchase.getPrice() * (double) purchase.getQuantity()))
                    .totalRewardPoints((int) (purchase.getPrice() * (double) purchase.getQuantity()))
                .build();
    }
}